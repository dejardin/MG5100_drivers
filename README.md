These directory contains modified CANON drivers for MG5100 which compile with FC26.
- The 3.40-26 branch is the genuine version from canon which compiles with newer version of libpng and cups
- The 3.80-26 is coming from https://github.com/spremi/cnijfilter-source-3.80 and modified to add backward support to MG5100/373

So you can :
- git clone https://gitlab.cern.ch/dejardin/MG5100_drivers
- create a tarball of the branch you want to use :<br> 
-- tar -cvzf cnijfilter-source-3.85-26.tar.gz cnijfilter-source-3.85-26 or<br>
-- tar -cvzf cnijfilter-source-3.40-26.tar.gz cnijfilter-source-3.40-26
- rebuild the corresponding rpm, asking for MG5100 support :<br>
-- rpmbuild -tb cnijfilter-source-3.40-26.tar.gz --define="MODEL mg5100" --define="MODEL_NUM 373" --with build_common_package
- And if everything is fine, install the rpm :<br>
-- dnf install /root/rpmbuild/RPMS/x86_64/cnijfilter*.rpm

- OK, it seems that only 3-40 is working for MG5100. If I find some time , I will investigate.

Creation : M.D. 2018/06/05<BR>
Last Mod : M.D. 2018/06/05
